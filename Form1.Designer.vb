﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSaveAutomatic
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnSaveAutoMAJ = New System.Windows.Forms.Button()
        Me.btnSelectDocPage = New System.Windows.Forms.Button()
        Me.txtChemDocPage = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'btnSaveAutoMAJ
        '
        Me.btnSaveAutoMAJ.Location = New System.Drawing.Point(122, 93)
        Me.btnSaveAutoMAJ.Name = "btnSaveAutoMAJ"
        Me.btnSaveAutoMAJ.Size = New System.Drawing.Size(181, 23)
        Me.btnSaveAutoMAJ.TabIndex = 9
        Me.btnSaveAutoMAJ.Text = "Charger et mettre à jour la table"
        Me.btnSaveAutoMAJ.UseVisualStyleBackColor = True
        '
        'btnSelectDocPage
        '
        Me.btnSelectDocPage.Location = New System.Drawing.Point(352, 27)
        Me.btnSelectDocPage.Name = "btnSelectDocPage"
        Me.btnSelectDocPage.Size = New System.Drawing.Size(98, 35)
        Me.btnSelectDocPage.TabIndex = 8
        Me.btnSelectDocPage.Text = "Choisir le dossier"
        Me.btnSelectDocPage.UseVisualStyleBackColor = True
        '
        'txtChemDocPage
        '
        Me.txtChemDocPage.Location = New System.Drawing.Point(97, 36)
        Me.txtChemDocPage.Name = "txtChemDocPage"
        Me.txtChemDocPage.Size = New System.Drawing.Size(249, 20)
        Me.txtChemDocPage.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Chemin du dossier"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'frmSaveAutomatic
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(462, 131)
        Me.Controls.Add(Me.btnSaveAutoMAJ)
        Me.Controls.Add(Me.btnSelectDocPage)
        Me.Controls.Add(Me.txtChemDocPage)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "frmSaveAutomatic"
        Me.Text = "Connexion au dossier d'image"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSaveAutoMAJ As System.Windows.Forms.Button
    Friend WithEvents btnSelectDocPage As System.Windows.Forms.Button
    Friend WithEvents txtChemDocPage As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
