﻿Imports System.Data.SQLite
Imports System
Imports System.IO
Imports System.ComponentModel


Public Class frmConsultPage
    Dim strConnexion As String = "data source=D:\SkyDrive\Documents\Visual Studio 2012\Projects\WF_Trichoptera_Malicky_Book\Trichoptera_Malicky_Page"
    Dim CON As New SQLiteConnection(strConnexion)
    Dim cmd As New SQLiteCommand("SELECT*  FROM EspTrichoLien ORDER BY pag_num", CON)
    Dim da As New SQLiteDataAdapter(cmd)
    Dim ds As New DataSet()
    Dim ds_rech As New DataSet()
    Private _scale As New SizeF(1, 1)
    Private _scaleDelta As New SizeF(0.01, 0.01) '1% for each wheel tick


#Region "Fonction et Procédure"
    Sub AfficheImage(ds_proc As DataSet, id_lig_select As Integer)
        Try

            'Appel de la fonction ByteArrayToImage que j'ai créé au dessous pour lire le tableau de byte et le transformer en image
            pctAffichePhoto1.Image = Image.FromFile(ds_proc.Tables("EspTrichoLien").Rows(id_lig_select)("imag_entier"))

        Catch ex As Exception
            pctAffichePhoto1.Image = Nothing
            'MessageBox.Show(ex.Message)
        End Try
    End Sub
    Sub MasquerColonne()
        'Pr masquer les colonnes
        For i = 4 To 10
            MonDataGridConsult.Columns(i).Visible = False
        Next
    End Sub

    Public Function ByteArrayToImage(ByVal ByteArray As Byte()) As Image
        Dim stream As New MemoryStream(ByteArray, 0, ByteArray.Length)
        Return Image.FromStream(stream, True)
    End Function

#End Region

    

    Private Sub frmConsultPage_Load(sender As Object, e As EventArgs) Handles Me.Load
        'Try
        CON.Open()
        da.Fill(ds, "EspTrichoLien")

        'Configuration du gestionnaire de liaison sur la source de données
        BindSourConsult.DataSource = ds.Tables("EspTrichoLien")
        BindNavigConsult.BindingSource = BindSourConsult

        'Liaison du gestionnaire avec les contrôles d'affichage de données

        MonDataGridConsult.DataSource = BindSourConsult

        If MonDataGridConsult.RowCount > 1 Then
            MonDataGridConsult.Rows(0).Selected = True
            Dim select_ligne As Integer = MonDataGridConsult.CurrentRow.Index
            AfficheImage(ds, select_ligne)
        End If
        

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try

    End Sub

    Private Sub TStxtRecherche_TextChanged(sender As Object, e As EventArgs) Handles TStxtRecherche.TextChanged

        Try
            If Not TStxtRecherche.Text = "" Then
                Dim Requete As String = "SELECT * FROM EspTrichoLien WHERE pag_num LIKE  '" & TStxtRecherche.Text & "%' ORDER BY pag_num"
                Dim Commande As New SQLiteCommand(Requete, CON)
                Dim Adaptateur As New SQLiteDataAdapter(Commande)
                ds_rech.Tables.Clear()
                Adaptateur.Fill(ds_rech, "EspTrichoLien")

                'Configuration du gestionnaire de liaison sur la source de données
                BindSourConsult.DataSource = ds_rech.Tables("EspTrichoLien")

                'Liaison du gestionnaire avec les contrôles d'affichage de données
                MonDataGridConsult.DataSource = BindSourConsult
                BindNavigConsult.BindingSource = BindSourConsult

                'Afficher l'image correspondante à la ligne selectionnée
                Dim select_ligne As Integer = MonDataGridConsult.CurrentRow.Index

                AfficheImage(ds_rech, select_ligne)
                Adaptateur.Dispose()
            Else

                'Configuration du gestionnaire de liaison sur la source de données
                BindSourConsult.DataSource = ds.Tables("EspTrichoLien")
                BindNavigConsult.BindingSource = BindSourConsult

                'Liaison du gestionnaire avec les contrôles d'affichage de données

                MonDataGridConsult.DataSource = BindSourConsult

                Dim select_ligne As Integer = MonDataGridConsult.CurrentRow.Index
                AfficheImage(ds, select_ligne)


            End If
            'cmd.Dispose()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Sub MonDataGridConsult_SelectionChanged(sender As Object, e As EventArgs) Handles MonDataGridConsult.SelectionChanged
        If TStxtRecherche.Text = "" Then
            If MonDataGridConsult.RowCount <> 0 Then
                Dim select_ligne As Integer = MonDataGridConsult.CurrentRow.Index
                AfficheImage(ds, select_ligne)
            End If
        Else
            Dim select_ligne As Integer = MonDataGridConsult.CurrentRow.Index
            AfficheImage(ds_rech, select_ligne)
        End If

    End Sub

    Private Sub ChoisirLeDossierToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChoisirLeDossierToolStripMenuItem.Click

        frmSaveAutomatic.Show()

        ds.Tables.Clear()
        da.Fill(ds, "EspTrichoLien")
        'Configuration du gestionnaire de liaison sur la source de données
        BindSourConsult.DataSource = ds.Tables("EspTrichoLien")
        BindNavigConsult.BindingSource = BindSourConsult

        'Liaison du gestionnaire avec les contrôles d'affichage de données

        MonDataGridConsult.DataSource = BindSourConsult
    End Sub

    Private Sub NouvelleBaseDeDonnéeToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles NouvelleBaseDeDonnéeToolStripMenuItem1.Click
        Dim confirm As String
        confirm = MsgBox("Voulez-vous créer une nouvelle base de donnée?", vbYesNo, "Confirmation")
        If confirm = vbYes Then
            SaveFileDialog1.Filter = "All Files|*.*|Sqlite (*.sqlite*)|*.sqlite| Sql (*.sql*) |*.sql| Access (*.mdb*) |*.mdb  "
            SaveFileDialog1.ShowDialog()

            If Not SaveFileDialog1.FileName = "" Then
                Dim DBFile As String
                SQLiteConnection.CreateFile(SaveFileDialog1.FileName)
                DBFile = "data source= '" & SaveFileDialog1.FileName & "'"

                Dim CON As New SQLiteConnection(DBFile)
                CON.Open()
                Dim myTableCreate As String = "CREATE TABLE TrichopteraBook(CustomerID INTEGER PRIMARY KEY , pag_num INTEGER ASC , imag_lien TEXT)"
                Dim afficheMyTable As String = "SELECT* FROM TrichopteraBook"
                Dim cmd As New SQLiteCommand(myTableCreate, CON)
                Dim cmd_affiche As New SQLiteCommand(afficheMyTable, CON)

                Dim ds As New DataSet
                Dim da As New SQLiteDataAdapter(cmd_affiche)

                cmd.ExecuteNonQuery()
                Dim confirm_affichage As String = MsgBox("Voulez-vous afficher la nouvelle table?", vbYesNo, "Affichage de la table")

                If confirm_affichage = vbYes Then

                    cmd_affiche.ExecuteNonQuery()

                    da.Fill(ds, "TrichopteraBook")

                    BindSourConsult.DataSource = ds.Tables("TrichopteraBook")
                    MonDataGridConsult.DataSource = BindSourConsult
                    BindNavigConsult.BindingSource = BindSourConsult

                    CON.Close()
                    cmd.Dispose()
                    cmd_affiche.Dispose()
                End If
            End If


        End If
    End Sub

    Private Sub QuitterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles QuitterToolStripMenuItem.Click
        Me.Close()

    End Sub
End Class