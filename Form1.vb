﻿Imports System.Data.SQLite
Imports System
Imports System.IO
Imports System.ComponentModel


Public Class frmSaveAutomatic
    Sub ConnexionTable()

    End Sub

    Private Sub btnSaveAutoSelectDoc_Click(sender As Object, e As EventArgs) Handles btnSelectDocPage.Click

        FolderBrowserDialog1.ShowDialog()
        txtChemDocPage.Text = FolderBrowserDialog1.SelectedPath
    End Sub


    Function NombreDossiers(ByVal Chemin As String) As Integer
        Return Directory.EnumerateDirectories(Chemin, "*", SearchOption.AllDirectories).Count()
    End Function



    Private Sub btnSaveAutoMAJ_Click(sender As Object, e As EventArgs) Handles btnSaveAutoMAJ.Click
        Dim listDocPage = Directory.EnumerateDirectories(txtChemDocPage.Text, "*", SearchOption.AllDirectories)
        Dim listFilePage = My.Computer.FileSystem.GetFiles(txtChemDocPage.Text)
        Dim nbSousDocsPage As Integer = listDocPage.Count
        Dim nbFilePage As Integer = listFilePage.Count
        Dim JusteNomFilePage As String

        'etablir la connexion a la base de donnée
        Dim strConnexion As String = "data source=D:\SkyDrive\Documents\Visual Studio 2012\Projects\WF_Trichoptera_Malicky_Book\Trichoptera_Malicky_Page"
        Dim CON As New SQLiteConnection(strConnexion)
       
        CON.Open()

        If frmConsultPage.MonDataGridConsult.RowCount <> 0 Then
            Dim SQL_DEL As String = "DELETE FROM EspTrichoLien"
            Dim cmd_del As New SQLiteCommand(SQL_DEL, CON)
            cmd_del.ExecuteNonQuery()
            cmd_del.Dispose()
        End If

        CON.Close()


        frmProgressBar.ProgressBar1.Minimum = 0
        frmProgressBar.ProgressBar1.Maximum = nbFilePage
        frmProgressBar.ProgressBar1.Value = 0
        frmProgressBar.ProgressBar1.Step = 1


        Me.Hide()
        frmProgressBar.Show()

        For j = 0 To nbFilePage - 1
            JusteNomFilePage = System.IO.Path.GetFileNameWithoutExtension(listFilePage(j))

            Try
                
               
                CON.Open()

                Dim strSQL As String = "INSERT INTO EspTrichoLien (id_esp,pag_num,imag_entier) VALUES (@id_esp,@pag_num,@imag_entier)"
                'Dim strSQL As String = "UPDATE EspTrichoLien SET pag_num=@pag_num,imag_entier=@imag_entier"
                Dim cmd As New SQLiteCommand(strSQL, CON)


                cmd.Parameters.AddWithValue("@id_esp", "id_" & JusteNomFilePage)
                cmd.Parameters.AddWithValue("@pag_num", CInt(Mid(JusteNomFilePage, 2)))
                cmd.Parameters.AddWithValue("@imag_entier", listFilePage(j))

                cmd.ExecuteNonQuery()
                cmd.Dispose()
                CON.Close()

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

            frmProgressBar.ProgressBar1.PerformStep()
            frmProgressBar.lblProgression.Text = j & " sur " & nbFilePage


        Next
       
        If frmProgressBar.ProgressBar1.Value = frmProgressBar.ProgressBar1.Maximum Then
            frmProgressBar.Close()

        End If

        Me.Close()



        'Try
        Dim cmd_sel As New SQLiteCommand("SELECT*  FROM EspTrichoLien ORDER BY pag_num", CON)
        Dim da_sel As New SQLiteDataAdapter(cmd_sel)
        Dim ds_sel As New DataSet()
        CON.Open()
        da_sel.Fill(ds_sel, "EspTrichoLien")

        'Configuration du gestionnaire de liaison sur la source de données
        frmConsultPage.BindSourConsult.DataSource = ds_sel.Tables("EspTrichoLien")
        frmConsultPage.BindNavigConsult.BindingSource = frmConsultPage.BindSourConsult

        'Liaison du gestionnaire avec les contrôles d'affichage de données

        frmConsultPage.MonDataGridConsult.DataSource = frmConsultPage.BindSourConsult

        If frmConsultPage.MonDataGridConsult.RowCount > 1 Then
            frmConsultPage.MonDataGridConsult.Rows(0).Selected = True
            Dim select_ligne As Integer = frmConsultPage.MonDataGridConsult.CurrentRow.Index
            frmConsultPage.AfficheImage(ds_sel, select_ligne)
        End If

        frmConsultPage.Refresh()
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try

    End Sub

    Sub BarDeChargement(indice As Integer)
        

    End Sub
End Class